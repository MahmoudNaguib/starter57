<?php

function factorialEven($n) {
    if ($n == 0) {
        return 1;
    }
    else {
        if ($n % 2 == 0) {
            return $n * factorialEven($n - 1);
        }
        else {
            return 1 * factorialEven($n - 1);
        }
    }
}

echo factorialEven(10);
