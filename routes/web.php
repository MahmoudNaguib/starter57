<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Route::get('/', function () {
  return view('welcome');
  });
 */
Route::group(['prefix'=>(app()->environment()=='testing')?'en':LaravelLocalization::setLocale(), 'middleware'=>[ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function() {
    AdvancedRoute::controller('auth', 'AuthController');
    Route::group(['middleware'=>['auth']], function() {
        AdvancedRoute::controller('profile', 'ProfileController');
        AdvancedRoute::controller('notifications', 'NotificationsController');
        Route::group(['middleware'=>['isAdmin'], 'prefix'=>'admin'], function() {
            AdvancedRoute::controller('roles', 'Admin\RolesController');
            AdvancedRoute::controller('countries', 'Admin\CountriesController');
            AdvancedRoute::controller('currencies', 'Admin\CurrenciesController');
            AdvancedRoute::controller('users', 'Admin\UsersController');
            AdvancedRoute::controller('items', 'Admin\ItemsController');
            AdvancedRoute::controller('sections', 'Admin\SectionsController');
            AdvancedRoute::controller('vendors', 'Admin\VendorsController');
            AdvancedRoute::controller('logs', 'Admin\LogsController');
            AdvancedRoute::controller('configs', 'Admin\ConfigsController');
            AdvancedRoute::controller('search', 'Admin\SearchController');
            AdvancedRoute::controller('translator', 'Admin\TranslatorController');
            AdvancedRoute::controller('options', 'Admin\OptionsController');
            AdvancedRoute::controller('ajax', 'Admin\AjaxController');
            AdvancedRoute::controller('/', 'Admin\DashBoardController');
        });
    });
    AdvancedRoute::controller('/', 'HomeController');
});


Route::prefix('api')->group(function () {
    require_once __DIR__ . '/api.php';
});
