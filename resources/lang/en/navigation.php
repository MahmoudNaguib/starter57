<?php 

return [
    '0' => '1',
    'Dashboard' => 'Dashboard',
    'Users' => 'Users',
    'Sections' => 'Sections',
    'Items' => 'Items',
    'Settings' => 'Settings',
    'Configurations' => 'Configurations',
    'Roles' => 'Roles',
    'translator' => 'translator',
    'Options' => 'Options',
    'Vendors' => 'Vendors',
    'Localization' => 'Localization',
];