<?php 

return [
    '0' => '1',
    'From' => 'From',
    'To' => 'To',
    'Search' => 'Search',
    'ID' => 'ID',
    'Action' => 'Action',
    'Module' => 'Module',
    'User' => 'User',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Field' => 'Field',
    'After' => 'After',
    'Before' => 'Before',
];