<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>
            <li class="nav-item {{(request()->is('*/items*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/items">
                    <i class="icon ion-ios-people-outline"></i>
                    <span>{{trans('navigation.Items')}}</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- container -->
</div>