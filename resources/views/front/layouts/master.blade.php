<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
        @include('front.partials.meta')
        @include('front.partials.css') @stack('css')
    </head>
    <body>
        <div class="slim-header">
            <div class="container">
                <div class="slim-header-left">
                    @include('front.partials.logo')
                    @include('front.partials.search')
                </div>
                <!-- slim-header-left -->
                <div class="slim-header-right">
                    @include('front.partials.notifications')
                    @include('front.partials.langSwitch')
                    @include('front.partials.user_navigation')
                </div>
                <!-- header-right -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-header -->
        @include('front.partials.navigation')
        <!-- slim-navbar -->
        <div class="slim-mainpanel">
            <div class="container">
                @include('front.partials.breadcrumb')
                @include('front.partials.flash_messages')
                <!-- section-wrapper -->
                @yield('content')
                <!-- section-wrapper -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-mainpanel -->
        <!-- slim-footer -->
        @include('front.partials.footer')
        @include('front.partials.js') @stack('js')
    </body>

</html>
