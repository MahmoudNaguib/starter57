@foreach(langs() as $lang)
    @include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('vendors.Title').' '.$lang,'placeholder'=>trans('vendors.Title'),'required'=>1]])
@endforeach
