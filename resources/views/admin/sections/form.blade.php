@foreach(langs() as $lang)
    @include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('countries.Title').' '.$lang,'placeholder'=>trans('countries.Title'),'required'=>1]])
@endforeach
