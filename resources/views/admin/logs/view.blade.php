@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('logs.Action')}}</td>
                <td width="75%" class="align-left">{{ucfirst($row->action)}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('logs.Module')}}</td>
                <td width="75%" class="align-left">{{ucfirst(config('key_modules.'.$row->loggable_type))}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('logs.User')}}</td>
                <td width="75%" class="align-left">{{$row->user->name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('logs.Created at')}}</td>
                <td width="75%" class="align-left">{{$row->created_at}}</td>
            </tr>
        </table>
    </div>
    @if($row->after)
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-15p">{{trans('logs.Field')}} </th>
                    <th class="wd-15p">{{trans('logs.After')}} </th>
                    <th class="wd-15p">{{trans('logs.Before')}} </th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($row->after as $key=>$value)
                <tr>
                    <td class="center"><b>{{ucfirst($key)}}</b></td>
                    <td class="center">{{@$row->after[$key]}}</td>
                    <td class="center">{{@$row->before[$key]}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @endif

</div>
@endsection
