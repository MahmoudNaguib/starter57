@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    {!! Form::open(['method' => 'get','files' => true] ) !!} 
    <div class="row mg-t-20 mg-b-20">
        <div class="col-sm-5 mg-t-10 mg-sm-t-0">
            {!! Form::text('from',request('from'),['class'=>'form-control datepicker','placeholder'=>trans('logs.From'),'id'=>'from_date']) !!}
        </div>
        <div class="col-sm-5 mg-t-10 mg-sm-t-0">
            {!! Form::text('to',request('to'),['class'=>'form-control datepicker','placeholder'=>trans('logs.To'),'id'=>'to_date']) !!}
        </div>
        <div class="col-sm-2">
            <input type="submit" class="btn btn-primary bd-0" value="{{ trans('logs.Search') }}">
        </div>
    </div>
    {!! Form::close() !!}
    @if(can('view-'.$module))
    @if (!$rows->isEmpty())
    <div class="table-responsive">
        <table class="table display responsive nowrap">
            <thead>
                <tr>
                    <th class="wd-10p">{{trans('logs.ID')}} </th>
                    <th class="wd-15p">{{trans('logs.Action')}} </th>
                    <th class="wd-15p">{{trans('logs.Module')}} </th>
                    <th class="wd-15p">{{trans('logs.User')}} </th>
                    <th class="wd-15p">{{trans('logs.Created at')}}</th>
                    <th class="wd-15p">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($rows as $row)
                <tr>
                    <td class="center"><a href="{{config('key_modules.'.$row->loggable_type).'/view/'.$row->loggable_id}}">{{$row->id}}</a></td>
                    <td class="center">{{ucfirst($row->action)}}</td>
                    <td class="center">{{ucfirst(config('key_modules.'.$row->loggable_type))}}</td>
                    <td class="center">{{$row->user->name}}</td>
                    <td class="center">{{$row->created_at}}</td>
                    <td class="center">
                        @if(can('view-'.$module))
                        <a class="btn btn-primary btn-xs" href="{{$module}}/view/{{$row->id}}" title="{{trans('logs.View')}}">
                            <i class="fa fa-eye"></i>
                        </a>
                        @endif

                        @if(can('delete-'.$module))
                        <a class="btn btn-danger btn-xs" href="{{$module}}/delete/{{$row->id}}" title="{{trans('logs.Delete')}}" data-confirm="{{trans('logs.Are you sure you want to delete this item')}}?">
                            <i class="fas fa-trash-alt"></i>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div>
        {!! $rows->appends(['from'=>request('from'),'to'=>request('to')])->render() !!}
    </div>
    @else
    {{trans("logs.There is no results")}}
    @endif
    @endif
</div>
@endsection
