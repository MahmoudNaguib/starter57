@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(request('q')) 

    @if(!@$users->isEmpty())
    <h3>{{trans('search.Users')}}</h3>
    <ul>
        @foreach($users as $row)
        <li><a href="users/view/{{$row->id}}" target="_blank">{{$row->name}}</a></li>
        @endforeach
    </ul>
    @endif

    @if(!@$sections->isEmpty())
    <h3>{{trans('search.Sections')}}</h3>
    <ul>
        @foreach($sections as $row)
        <li><a href="sections/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif

    @if(!@$items->isEmpty())
    <h3>{{trans('search.Items')}}</h3>
    <ul>
        @foreach($items as $row)
        <li><a href="items/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
        @endforeach
    </ul>
    @endif

    @endif
</div>
@endsection
