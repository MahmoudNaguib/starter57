<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/admin">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>
            @if(can('create-sections') || can('view-sections'))
            <li class="nav-item {{(request()->is('*/sections*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/sections">
                    <i class="icon fa custom-fa fa-tasks"></i>
                    <span>{{trans('navigation.Sections')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-vendors') || can('view-vendors'))
            <li class="nav-item {{(request()->is('*/vendors*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/vendors">
                    <i class="icon fa custom-fa fa-tasks"></i>
                    <span>{{trans('navigation.Vendors')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-users') || can('view-users'))
            <li class="nav-item {{(request()->is('*/users*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/users">
                    <i class="icon fa ion-ios-contact-outline"></i>
                    <span>{{trans('navigation.Users')}}</span>
                </a>
            </li>
            @endif

            @if(@auth()->user()->is_super_admin)
            <li class="nav-item with-sub settings">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>{{trans('navigation.Settings')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li class="{{(request()->is('*/configs*'))?"active":""}}">
                            <a href="{{lang()}}/admin/configs">{{trans('navigation.Configurations')}}</a>
                        </li>
                        <li class="{{(request()->is('*/translator*'))?"active":""}}">
                            <a href="{{lang()}}/admin/translator">{{trans('navigation.Localization')}}</a>
                        </li>
                        <li class="{{(request()->is('*/roles*'))?"active":""}}">
                            <a href="{{lang()}}/admin/roles">{{trans('navigation.Roles')}}</a>
                        </li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif
            
        </ul>
    </div>
    <!-- container -->
</div>