@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('items.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('items.User')}}</td>
                <td width="75%" class="align-left">{{$row->user->name}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('items.Section')}}</td>
                <td width="75%" class="align-left">{{$row->section->title}}</td>
            </tr>
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('items.Title')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('title',$lang)}}</td>
            </tr>
            @endforeach
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('items.Summary')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{ @$row->getTranslation('summary',$lang) }}</td>
            </tr>
            @endforeach
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('items.Content')}} {{$lang}}</td>
                <td width="75%" class="align-left">{!! @$row->getTranslation('content',$lang) !!}</td>
            </tr>
            @endforeach

            <tr>
                <td width="25%" class="align-left">{{trans('items.Price')}}</td>
                <td width="75%" class="align-left">{{@$row->price}} {{@$row->currency->iso}}</td>
            </tr>

            <tr>
                <td width="25%" class="align-left">{{trans('items.Image')}}</td>
                <td width="75%" class="align-left">{!! viewImage($row->image,'small') !!}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('items.Is active')}}</td>
                <td width="75%" class="align-left"><img src="img/{{($row->is_active)?'check.png':'close.png'}}"></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('items.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>

        </table>
    </div>
</div>
@endsection
