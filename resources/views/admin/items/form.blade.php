@include('form.select',['name'=>'vendor_id','options'=>$row->getVendors(),'attributes'=>['class'=>'form-control select2','id'=>'vendor_id','label'=>trans('items.Vendor'),'placeholder'=>trans('items.Select Vendor'),'required'=>1]])
@include('form.select',['name'=>'user_id','options'=>[],'attributes'=>['class'=>'form-control select2','id'=>'user_id','label'=>trans('items.User'),'required'=>1]])


@include('form.select',['name'=>'section_id','options'=>$row->getSections(),'attributes'=>['class'=>'form-control select2','label'=>trans('items.Section'),'placeholder'=>trans('items.Select Section'),'required'=>1]])

@foreach(langs() as $lang)
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>['class'=>'form-control','label'=>trans('items.Title').' '.$lang,'placeholder'=>trans('items.Title'),'required'=>1]])
@endforeach

@foreach(langs() as $lang)
@include('form.input',['name'=>'summary['.$lang.']','value'=>$row->getTranslation('summary',$lang),'type'=>'textarea','attributes'=>['class'=>'form-control','label'=>trans('items.Summary').' '.$lang,'placeholder'=>trans('items.Summary'),'required'=>1]])
@endforeach


@foreach(langs() as $lang)
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>['class'=>'form-control editor','label'=>trans('items.Content').' '.$lang,'placeholder'=>trans('items.Content'),'required'=>1]])
@endforeach


@include('form.select',['name'=>'currency_id','options'=>$row->getCurrencies(),'attributes'=>['class'=>'form-control','label'=>trans('items.Currency'),'placeholder'=>trans('items.Currency'),'required'=>1]])

@include('form.input',['name'=>'price','type'=>'number','attributes'=>['class'=>'form-control','label'=>trans('items.Price'),'placeholder'=>trans('items.Price'),'step'=>'0.01','required'=>1,'min'=>0,'pattern'=>'^\d*(\.\d{0,2})?$']])

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('items.Image'),'placeholder'=>trans('items.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('items.Is active')]])


@push('js')
<script>
    $(document).ready(function () {
        $("#vendor_id").change(function () {
            var vendor_id = $(this).val();
            if (vendor_id != '' && vendor_id != undefined) {
                $.ajax({
                    url: 'ajax/user-by-vendor',
                    type: 'get',
                    data: {vendor_id: vendor_id},
                    dataType: 'json',
                    success: function (response) {
                        var $el = $("#user_id");
                        $el.empty(); // remove old options
                        $.each(response, function (value, key) {
                            $el.append($("<option></option>")
                                    .attr("value", value).text(key));
                        });
                    }
                });
            }
        });
        $("#vendor_id").trigger('change');
    });

</script>
@endpush