@extends('layouts.notifications')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            <tr>
                <td width="25%" class="align-left">{{trans('notifications.Message')}}</td>
                <td width="75%" class="align-left">{{@$row->message}}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('notifications.Url')}}</td>
                <td width="75%" class="align-left"><a href="{{$row->url}}" target="_blank">{{str_limit($row->url,20)}}</a></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('notifications.Created at')}}</td>
                <td width="75%" class="align-left">{{@$row->created_at}}</td>
            </tr>
        </table>
    </div>
</div>
@endsection
