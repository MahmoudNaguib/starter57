<?php
return[
    'admin\users'=>['edit', 'view', 'delete', 'create'],
    'admin\countries'=>['create', 'edit', 'view', 'delete'],
    'admin\currencies'=>['create', 'edit', 'view', 'delete'],
    'admin\sections'=>['create', 'edit', 'view', 'delete'],
    'admin\vendors'=>['create', 'edit', 'view', 'delete'],
    'admin\items'=>['create', 'edit', 'view', 'delete'],
    'admin\options'=>['create', 'edit', 'view', 'delete'],
    'admin\translator'=>['edit', 'view'],
];
