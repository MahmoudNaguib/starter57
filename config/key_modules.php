<?php
return[
    'App\Models\User'=>'users',
    'App\Models\Country'=>'countries',
    'App\Models\Currency'=>'currencies',
    'App\Models\Item'=>'items',
    'App\Models\Section'=>'sections',
    'App\Models\Option'=>'options',
    'App\Models\Role'=>'roles',
];
