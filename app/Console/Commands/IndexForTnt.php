<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class IndexForTnt extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Index all models for tnt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->call('scout:import App\\Models\\Child');
    }

}
