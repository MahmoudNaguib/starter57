<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends BaseModel {
    use SoftDeletes,
        \App\Models\Traits\Logger,
        \App\Models\Traits\CreatedBy,
        \Spatie\Translatable\HasTranslations;
///////////////////////////// has translation
    public $translatable=['title'];
    protected $table="currencies";
    protected $guarded=[
        'deleted_at',
        'logged_user'
    ];
    protected $hidden=[
        'deleted_at',
    ];
    public $rules=[
        'iso'=>'required|size:3',
        'title'=>'required',
        'rate'=>'required|numeric'
    ];

    public static function boot() {
        parent::boot();
        static::saved(function ($row) {
            \Cache::forget('currencies');
        });
        static::updated(function ($row) {
            \Cache::forget('currencies');
        });
        static::created(function ($row) {
            \Cache::forget('currencies');
        });
        static::deleted(function ($row) {
            \Cache::forget('currencies');
        });
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if($rows) {
            foreach($rows as $row) {
                unset($object);
                $object['id']=$row->id;
                $object['Short code']=$row->iso;
                $object['Title']=$row->title;
                $object['Rate']=$row->rate;
                $object['Created at']=$row->created_at;
                $labels=array_keys($object);
                $data[]=$object;
            }
            export($data, $labels, $fileName);
        }
    }
}