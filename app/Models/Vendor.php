<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends BaseModel {
    use SoftDeletes,
        \App\Models\Traits\Logger,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Spatie\Translatable\HasTranslations;
    ///////////////////////////// has translation
    public $translatable=['title'];
    ///////////////////////////// has attach
    protected static $attachFields=[
        'image'=>[
            'sizes'=>['small'=>'crop,100x100', 'large'=>'resize,800x600'],
            'path'=>'uploads'
        ],
    ];
    protected $table="vendors";
    protected $guarded=[
        'deleted_at',
        'logged_user'
    ];
    protected $hidden=[
        'deleted_at',
    ];
    public $rules=[
        'title'=>'required',
    ];

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if($rows) {
            foreach($rows as $row) {
                unset($object);
                $object['id']=$row->id;
                $object['Title']=@$row->title;
                $object['Created at']=$row->created_at;
                $labels=array_keys($object);
                $data[]=$object;
            }
            export($data, $labels, $fileName);
        }
    }
}