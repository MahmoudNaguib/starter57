<?php

namespace App\Models;

class Token extends BaseModel {

    protected $table = "tokens";
    protected $guarded = [
        'logged_user',
    ];
    protected $hidden = [
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id')->withTrashed()->withDefault();
    }

}
