<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\Logger,
        \App\Models\Traits\CreatedBy,
        \Spatie\Translatable\HasTranslations;
    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "countries";
    protected $guarded = [
        'deleted_at',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'iso' => 'required|size:2',
        'title' => 'required',
    ];

    public function getNameAttribute() {
        return @$this->getTranslation('title', lang()) . ' (' . @$this->attributes['iso'] . ')';
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Short code'] = $row->iso;
                $object['Title'] = @$row->title;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
