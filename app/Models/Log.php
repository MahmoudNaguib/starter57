<?php

namespace App\Models;

class Log extends BaseModel {

    protected $table = "logs";
    protected $guarded = [
        'deleted_at',
    ];
    protected $hidden = [
        'deleted_at',
        'logged_user'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function ($model) {
            $model->setCreatedAt($model->freshTimestamp());
        });
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

    public function loggable() {
        return $this->morphTo();
    }

    public function scopeAction($q, $action) {
        return $q->where('action', $action);
    }

    public function scopeWasDeleted($q) {
        return $q->action('deleted');
    }

    public function scopeWasUpdated($q) {
        return $q->action('updated');
    }

    public function scopeWasCreated($q) {
        return $q->action('created');
    }

    public function scopeBetween($q, $start, $end) {
        return $q->whereBetween('created_at', [$start, $end]);
    }

    public function scopeEntity($q, $loggableType, $loggableId) {
        return $q->where('loggable_type', $loggableType)
                        ->where('loggable_id', $loggableId);
    }

    public function scopeStateOn($q, $datetime) {
        $query = clone ($q);
        $class = $q->first()->loggable_type;
        $attrs = $q->wasCreated()->first()->after;
        $changes = $query->wasUpdated()
                ->where('created_at', '<=', $datetime)
                ->get();
        foreach ($changes as $change) {
            $attrs = array_merge($attrs, $change->after);
        }
        return new $class($attrs);
    }

    public function getBeforeAttribute($value) {
        return $value ? (array) json_decode($value) : null;
    }

    public function getAfterAttribute($value) {
        return $value ? (array) json_decode($value) : null;
    }

    public function getData() {
        return $this->with(['user'])->when(request('from'), function($q) {
                    $q->where('created_at', '>=', request('from'));
                })->when(request('to'), function($q) {
                    $q->where('created_at', '<', date('Y-m-d', strtotime(request('to') . ' + 1 day')));
                })->latest();
    }

}
