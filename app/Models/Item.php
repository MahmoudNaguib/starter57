<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends \App\Models\BaseModel {
    use SoftDeletes,
        \App\Models\Traits\Logger,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;
    ///////////////////////////// has translation
    public $translatable=['title', 'summary', 'content'];
    protected $table="items";
    protected $guarded=[
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden=[
        'deleted_at',
    ];
    protected static $attachFields=[
        'image'=>[
            'sizes'=>['small'=>'crop,100x100', 'large'=>'resize,800x600'],
            'path'=>'uploads'
        ],
    ];
    public $rules=[
        'vendor_id'=>'required',
        'user_id'=>'required',
        'section_id'=>'required',
        'title'=>'required',
        'summary'=>'required',
        'content'=>'required',
        'currency_id'=>'required',
        'price'=>'required|numeric',
        'image'=>'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array=[
            'id'=>$this->id,
            'title'=>$this->title,
            'summary'=>$this->title,
        ];
        return $array;
    }

    public function getVendors() {
        return \App\Models\Vendor::pluck('title', 'id');
    }

    public function getUsers() {
        return \App\Models\User::where('is_vendor', 1)->pluck('name', 'id');
    }

    public function getSections() {
        return \App\Models\Section::pluck('title', 'id');
    }

    public function section() {
        return $this->belongsTo(Section::class, 'section_id')->withTrashed()->withDefault();
    }

    public function currency() {
        return $this->belongsTo(Currency::class, 'currency_id')->withTrashed()->withDefault();
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id')->withTrashed()->withDefault();
    }

    public function vendor() {
        return $this->belongsTo(Vendor::class, 'vendor_id')->withTrashed()->withDefault();
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getData() {
        return $this->with(['user', 'section'])
                        ->when(request('user_id'), function($q) {
                            return $q->where('user_id', request('user_id'));
                        })
                        ->when(request('section_id'), function($q) {
                            return $q->where('section_id', request('section_id'));
                        });
    }

    public function export($rows, $fileName) {
        if($rows) {
            foreach($rows as $row) {
                unset($object);
                $object['id']=$row->id;
                $object['Section']=$row->section->title;
                $object['Title']=$row->title;
                $object['Summary']=$row->summary;
                $object['Content']=$row->content;
                $object['Is Active']=$row->is_active;
                $object['Created at']=$row->created_at;
                $labels=array_keys($object);
                $data[]=$object;
            }
            export($data, $labels, $fileName);
        }
    }
}