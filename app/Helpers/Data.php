<?php

////////////// Default Data
function insertDefaultTokens() {
    $rows = [
        [
            'user_id' => 4,
            'token' => 'sf13bf6d4241be097a975f718adbb179c81e728d9d4c2f636f067f89cc14862c5e33b9abc68925efdc88fe95bf7ac767',
            'expiry_at' => date('Y-m-d', strtotime("+" . env('TOKEN_EXPIRATION', 100) . " days")),
            'device' => 'anoynomous',
            'device_id' => 'anoynomous'
        ]
    ];
    \DB::table('tokens')->insert($rows);
}

function insertDefaultRoles() {
    $rows = [
        [
            'id' => 1,
            'title' => 'Super Administrator',
            'permissions' => json_encode(permissions()),
            'created_by' => 2,
            'is_default' => 1,
        ]
    ];
    \DB::table('roles')->insert($rows);
}

function insertDefaultVendors() {
    foreach (langs() as $lang) {
        $title[$lang] = 'Vendor 1';
    }
    $title = json_encode($title);
    $rows = [
        [
            'id' => 1,
            'title' => $title,
            'created_by' => 2,
        ]
    ];
    \DB::table('vendors')->insert($rows);
}

function insertDefaultUsers() {
    $users = [
        [
            'id' => 1,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => 1,
            'vendor_id' => Null,
            'name' => env('SUPER_ADMIN_NAME', 'Super Admin'),
            'email' => env('SUPER_ADMIN_EMAIL', 'super@admin.com'),
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage(env('SUPER_ADMIN_NAME', 'Super Admin')),
            'created_by' => 2,
        ],
        [
            'id' => 2,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => 1,
            'vendor_id' => Null,
            'name' => env('ADMIN_NAME', 'Admin'),
            'email' => env('ADMIN_EMAIL', 'admin@admin.com'),
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage(env('ADMIN_NAME', 'Admin')),
            'created_by' => 2,
        ],
        [
            'id' => 3,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => NULL,
            'vendor_id' => 1,
            'name' => 'Vendor User',
            'email' => 'user@vendor.com',
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage('Vendor User'),
            'created_by' => 2,
        ],
        [
            'id' => 4,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => NULL,
            'vendor_id' => Null,
            'name' => 'Guest User',
            'email' => 'user@user.com',
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage('Guest User'),
            'created_by' => 2,
        ]
    ];
    \DB::table('users')->insert($users);
}

function insertDefaultConfigs() {
    \Cache::forget('configs');
    @copy(public_path() . '/img/logo.png', public_path() . '/uploads/small/logo.png');
    @copy(public_path() . '/img/logo.png', public_path() . '/uploads/large/logo.png');
    $rows = [];
    //////////// $appName
    $appName = env('APP_NAME');
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Basic Information',
            'field' => 'application_name',
            'label' => 'Application Name',
            'value' => $appName,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    //////////// $welcome
    $welcome = 'Welcome to our application';
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => '',
            'type' => 'Basic Information',
            'field' => 'welcome',
            'label' => 'Welcome Message',
            'value' => $welcome,
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////// Logo
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Basic Information',
        'field' => 'logo',
        'label' => 'Logo',
        'value' => 'logo.png',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// Email
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'email',
        'label' => 'Email',
        'value' => env('CONTACT_EMAIL', 'contact@starter57.com'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// Phone
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'phone',
        'label' => 'Phone',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// Mobile
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'mobile',
        'label' => 'Mobile',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// longitude
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'longitude',
        'label' => 'Location (longitude)',
        'value' => '31.324104799999986',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// latitude
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'latitude',
        'label' => 'Location (latitude)',
        'value' => '30.0685382',
        'lang' => NULL,
        'created_by' => 2,
    ];
    \DB::table('configs')->insert($rows);
}

function insertDefaultOptions() {
    $rows = [
        
    ];
    if ($rows) {
        foreach ($rows as &$row) {
            foreach (langs() as $lang) {
                $title[$lang] = $row['title'];
            }
            $row['title'] = json_encode($title);
        }
    }
    Illuminate\Support\Facades\DB::table('options')->insert($rows);
}
