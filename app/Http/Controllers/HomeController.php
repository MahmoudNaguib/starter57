<?php

namespace App\Http\Controllers;

class HomeController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'home';
        $this->views='front.'.$this->module;
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Home');
        return view($this->views . '.index', $data);
    }
}