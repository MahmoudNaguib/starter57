<?php

namespace App\Http\Controllers;

class ProfileController extends Controller {
    public $model;
    public $module;

    public function __construct(\App\Models\User $model) {
        $this->module='profile';
        $this->model=$model;
        $this->rules=$model->rules;
    }

    public function getEdit() {
        $data['module'] = $this->module;
        $data['row']=$this->model->findOrFail(auth()->user()->id);
        $data['page_title']=trans('profile.Edit Profile');
        
        return view($this->module.'.edit', $data);
    }

    public function postEdit() {
        $row=$this->model->findOrFail(auth()->user()->id);
        unset($this->rules['branches'], $this->rules['role_id'], $this->rules['profileable_id']);
        $this->rules['email'].=','.$row->id.',id,deleted_at,NULL';
        $this->rules['password']='nullable|confirmed|min:8';
        $this->validate(request(), $this->rules);
        if($row->update(request()->except(['password_confirmation']))) {
            flash(trans('app.Update successfully'))->success();
            return back();
        }
    }

    public function getLogout() {
        auth()->logout();
        session()->forget(['default_currency']);
        return back();
    }
}