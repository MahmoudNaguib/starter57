<?php

namespace App\Http\Controllers\Admin;

class LogsController extends \App\Http\Controllers\Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Log $model) {
        $this->module='admin/logs';
        $this->title=trans('app.Logs');
        $this->model=$model;
    }

    public function getIndex() {
        $data['module']=$this->module;
        $data['page_title']=trans('app.List')." ".$this->title;
        $data['rows']=$this->model->getData()->paginate(env('PAGE_LIMIT', 20));
        return view($this->module.'.index', $data);
    }

    public function getView($id) {
        authorize('view-'.$this->module);
        $data['module']=$this->module;
        $data['page_title']=trans('app.View')." ".$this->title;
        $data['breadcrumb']=[$this->title=>$this->module];
        $data['row']=$this->model->findOrFail($id);
        return view($this->module.'.view', $data);
    }

    public function getDelete($id) {
        authorize('delete-'.$this->module);
        $row=$this->model->findOrFail($id);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return redirect(lang().'/'.$this->module);
    }
}