<?php

namespace App\Http\Controllers\Admin;
use Spatie\Searchable\Search;

class SearchController extends \App\Http\Controllers\Controller {

    public function __construct() {
        $this->module = 'admin/search';
        $this->title = trans('app.Search results');
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = $this->title;
        if(request('q')) {
            $data['users'] = \App\Models\User::search(request('q'))->get();
            $data['items'] = \App\Models\Item::search(request('q'))->get();
            $data['sections'] = \App\Models\Section::search(request('q'))->get();
        }
        return view($this->module.'.index', $data);
    }
}