<?php

namespace App\Http\Controllers\Admin;

class AjaxController extends \App\Http\Controllers\Controller {
    public $model;
    public $module;

    public function __construct() {
        $this->middleware(['auth']);
    }

    public function getUserByVendor() {
        if(request('vendor_id')) {
            $rows=\App\Models\User::active()->where('vendor_id', request('vendor_id'))->pluck('name', 'id');
            return response()->json($rows, 200);
        }
    }
}