<?php

namespace App\Http\Controllers;

class NotificationsController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\Notification $model) {
        $this->module = 'notifications';
        $this->title = trans('app.Notifications');
        $this->model = $model;
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.List') . " " . $this->title;
        $data['rows'] = $this->model->getData()->get();
        return view($this->module . '.index', $data);
    }

    public function getTo($id) {
        $row = $this->model->findOrFail($id);
        $row->seen_at = date("Y-m-d H:i:s");
        $row->save();
        if ($row->url) {
            return redirect($row->url);
        }
        return redirect(lang() . '/' . $this->module);
    }

    public function getReadAll($id = null) {
        if (!$id) {
            $id = auth()->user()->id;
        }
        $this->model->where('to_id', $id)->update(['seen_at' => date("Y-m-d H:i:s")]);
        flash()->success(trans('app.All mesages marked as readed'));
        return back();
    }

    public function getView($id) {
        $data['module'] = $this->module;
        $data['page_title'] = trans('app.View') . " " . $this->title;
        $data['breadcrumb'] = [$this->title => $this->module];
        $data['row'] = $this->model->where('to_id', auth()->user()->id)->where('id', $id)->first();
        if (!$data['row'])
            return abort(404);
        $data['row']->seen_at = date("Y-m-d H:i:s");
        $data['row']->save();
        return view($this->module . '.view', $data);
    }

    public function getDelete($id) {
        $row = $this->model->where('to_id', auth()->user()->id)->where('id', $id)->first();
        if (!$row)
            return abort(404);
        $row->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

    public function getDeleteAll() {
        $this->model->where('to_id', auth()->user()->id)->delete();
        flash()->success(trans('app.Deleted Successfully'));
        return back();
    }

}
