<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        foreach (langs() as $lang) {
            $title[$lang] = $this->getTranslation('title', $lang);
            $summary[$lang] = $this->getTranslation('summary', $lang);
            $content[$lang] = $this->getTranslation('content', $lang);
        }
        return [
            'type' => 'items',
            'id' => $this->id,
            'attributes' => [
                'title' => $title,
                'summary' => $summary,
                'content' => $content,
                'price'=>$this->when(isset($this->price), $this->price),
            ],
            'relationships' => [
                'creator'=>new UserResource($this->whenLoaded('creator')),
            ]
        ];
    }

}
