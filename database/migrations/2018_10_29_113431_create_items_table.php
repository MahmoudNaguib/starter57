<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vendor_id')->nullable()->index()->default(1); //required
            $table->bigInteger('user_id')->nullable()->index()->default(1); //required
            $table->bigInteger('section_id')->nullable()->index()->default(1); //required
            $table->text('title')->nullable();  //required
            $table->text('summary')->nullable();  //required
            $table->text('content')->nullable(); // required
            $table->bigInteger('currency_id')->nullable()->index()->default(1); //required
            $table->float('price', 11, 2)->nullable()->default(0)->index(); // required|numeric
            $table->string('image')->nullable();
            $table->boolean('is_active')->nullable()->default(1)->index();
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('items');
    }
}