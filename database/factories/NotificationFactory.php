<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Notification::class, function (Faker $faker) {
    $ids = App\Models\User::pluck('id')->toArray();
    return [
        'from_id' => $ids[array_rand($ids)],
        'to_id' => $ids[array_rand($ids)],
        'message' => $faker->sentence(8),
        'url' => $faker->url(),
        'seen_at' => Null,
    ];
});
