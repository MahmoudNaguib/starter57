<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Currency::class, function (Faker $faker) {
    return [
        'iso' => str_random(3),
        'title' => $faker->city(),
        'rate'=>  rand(1, 20),
        'created_by'=>2
    ];
});
