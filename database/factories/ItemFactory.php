<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Item::class, function (Faker $faker) {
    foreach(langs() as $lang) {
        $title[$lang]=$faker->sentence(4);
        $summary[$lang]=$faker->sentence(10);
        $content[$lang]=$faker->paragraph();
    }
    $sectionIds =App\Models\Section::pluck('id')->toArray();
    return [
        'vendor_id'=>1,
        'user_id'=>3,
        'section_id' => $sectionIds[array_rand($sectionIds)],
        'title'=>$title,
        'summary'=>$summary,
        'content'=>$content,
        'is_active'=>1,
        'created_by'=>2
    ];
});
