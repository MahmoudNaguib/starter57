<?php

use Faker\Generator as Faker;
use function GuzzleHttp\json_decode;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */
$factory->define(App\Models\Log::class, function (Faker $faker) {
    $ids = App\Models\Child::pluck('id')->toArray();
    $id = array_rand($ids);

    return [
        'loggable_id' => $ids[$id],
        'loggable_type'=>'App\Models\Child',
        'action' => 'created',
    ];
});
