<?php
use Illuminate\Database\Seeder;

class VendorsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('vendors')->delete();
            DB::statement("ALTER TABLE vendors AUTO_INCREMENT = 1");
            factory(App\Models\Vendor::class, 5)->create();
        }
    }
}