<?php
use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('items')->delete();
            DB::statement("ALTER TABLE items AUTO_INCREMENT = 1");
            factory(App\Models\Item::class, 50)->create();
        }
    }
}