<?php

use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('currencies')->delete();
            DB::statement("ALTER TABLE currencies AUTO_INCREMENT = 1");
            $rows = [
                [
                    'id' => 1,
                    'iso' => 'EGP',
                    'title' => 'Egyptian Pound',
                    'rate' => 1,
                    'created_by' => 2,
                ],
                [
                    'id' => 2,
                    'iso' => 'USD',
                    'title' => 'US Dollar',
                    'rate' => 17.58,
                    'created_by' => 2,
                ],
                [
                    'id' => 3,
                    'iso' => 'EUR',
                    'title' => 'Euro',
                    'rate' => 19.86,
                    'created_by' => 2,
                ],
                [
                    'id' => 4,
                    'iso' => 'SAR',
                    'title' => 'Suadi Riyal',
                    'rate' => 4.68,
                    'created_by' => 2,
                ],
                [
                    'id' => 5,
                    'iso' => 'AED',
                    'title' => 'United Arab Emirates Dirham',
                    'rate' => 4.78,
                    'created_by' => 2,
                ],
                
            ];
            if ($rows) {
                foreach ($rows as &$row) {
                    foreach (langs() as $lang) {
                        $title[$lang] = $row['title'];
                    }
                    $row['title'] = json_encode($title);
                }
            }
            DB::table('currencies')->insert($rows);
        }
    }

}
