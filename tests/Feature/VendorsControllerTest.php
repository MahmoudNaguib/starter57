<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VendorsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_vendors() {
        dump('test_list_vendors');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Vendor::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/vendors')
                ->assertStatus(200)
                ->assertSee('vendors');
    }

    public function test_create_vendors() {
        dump('test_create_vendors');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Vendor::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/vendors/create', $row->toArray());
        $latest = \App\Models\Vendor::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_vendors() {
        dump('test_edit_vendors');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Vendor::create(factory(\App\Models\Vendor::class)->make()->toArray());
        $row = factory(\App\Models\Vendor::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/vendors/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Vendor::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_vendors() {
        dump('test_delete_vendors');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Vendor::create(factory(\App\Models\Vendor::class)->make()->toArray());
        $row = factory(\App\Models\Vendor::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/vendors/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
