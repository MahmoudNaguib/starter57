<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventsTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_events() {
        dump('test_list_events');
        $user = \App\Models\User::find(2);
        $this->actingAs($user)
                ->get('api/events')
                ->assertStatus(200)
                ->assertSee('events');
    }

    public function test_create_events() {
        dump('test_create_events');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)->post('api/events', $row->toArray())
                ->assertStatus(201)
                ->assertSee('events')
                ->assertSeeText($row->title);
        \App\Models\Event::find($response->baseResponse->original->id)->forceDelete();
    }

    public function test_edit_events() {
        dump('test_edit_events');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Event::create(factory(\App\Models\Event::class)->make()->toArray());
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)->put('api/events/' . $record->id, $row->toArray())
                ->assertStatus(200)
                ->assertSee('events')
                ->assertSeeText($row->title);
        $record->forceDelete();
    }

    public function test_delete_events() {
        dump('test_delete_events');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Event::create(factory(\App\Models\Event::class)->make()->toArray());
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)->delete('api/events/' . $record->id, $row->toArray())
                ->assertStatus(204);
        $record->forceDelete();
    }

}
