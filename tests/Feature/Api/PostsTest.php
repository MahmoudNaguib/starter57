<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_posts() {
        dump('test_list_posts');
        $user = \App\Models\User::find(2);
        $this->actingAs($user)
                ->get('api/posts')
                ->assertStatus(200)
                ->assertSee('posts');
    }

    public function test_create_posts() {
        dump('test_create_posts');
        $user = \App\Models\User::find(2);
        $row=  factory(\App\Models\Post::class)->make();
        $response=$this->actingAs($user)->post('api/posts', $row->toArray())
                ->assertStatus(201)
                ->assertSee('posts')
                ->assertSeeText($row->title);
        \App\Models\Post::find($response->baseResponse->original->id)->forceDelete();
    }
    public function test_edit_posts() {
        dump('test_edit_posts');
        $user = \App\Models\User::find(2);
        $record=  \App\Models\Post::create(factory(\App\Models\Post::class)->make()->toArray());
        $row=  factory(\App\Models\Post::class)->make();
        $response= $this->actingAs($user)->put('api/posts/'.$record->id, $row->toArray())
                ->assertStatus(200)
                ->assertSee('posts')
                ->assertSeeText($row->title);
        $record->forceDelete();
    }
    public function test_delete_posts() {
        dump('test_delete_posts');

        $user = \App\Models\User::find(2);
        $record=  \App\Models\Post::create(factory(\App\Models\Post::class)->make()->toArray());
        $row=  factory(\App\Models\Post::class)->make();
        $response= $this->actingAs($user)->delete('api/posts/'.$record->id, $row->toArray())
                ->assertStatus(204);
        $record->forceDelete();
    }

}
