<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParentChildrenTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_parent_children() {
        dump('test_list_parent_children');
        $user = \App\Models\User::find(3);
        $this->actingAs($user)
                ->get('api/parents/children')
                ->assertStatus(200)
                ->assertSee('children');
    }

     /**
     * A basic test example.
     *
     * @return void
     */
    public function test_show_parent_children() {
        dump('test_show_parent_children');
        $user = \App\Models\User::find(3);
        $record= \App\Models\ChildParent::where('parent_id',$user->profileable_id)->first();
        $this->actingAs($user)
            ->get('api/parents/children/'. $record->id)
            ->assertStatus(200)
            ->assertSee('children');
        $row = \App\Models\Child::find($record->id);
        $this->assertEquals($record->id, $row->id);
    }

}
