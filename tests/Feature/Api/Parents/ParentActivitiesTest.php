<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParentActivitiesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_parent_activities() {
        dump('test_list_parent_activities');
        $user = \App\Models\User::find(3);
        $this->actingAs($user)
                ->get('api/parents/activities')
                ->assertStatus(200)
                ->assertSee('activities');
    }

     /**
     * A basic test example.
     *
     * @return void
     */
    public function test_show_parent_activities() {
        dump('test_show_parent_activities');
        $user = \App\Models\User::find(3);
        $child = \App\Models\ChildParent::where('parent_id',$user->profileable_id)->first();
        $record = \App\Models\Activity::create(factory(\App\Models\Activity::class)->make(['child_id'=>$child->child_id,'notify_parent'=>1])->toArray());
        $this->actingAs($user)
            ->get('api/parents/activities/'. $record->id)
            ->assertStatus(200)
            ->assertSee('activities');
        $row = \App\Models\Activity::find($record->id);
        $this->assertEquals($record->child_id, $row->child_id);
        $record->forceDelete();
    }

}
