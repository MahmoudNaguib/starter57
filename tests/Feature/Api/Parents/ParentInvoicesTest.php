<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParentInvoicesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_parent_invoices() {
        dump('test_list_parent_invoices');
        $user = \App\Models\User::find(3);
        $this->actingAs($user)
                ->get('api/parents/invoices')
                ->assertStatus(200)
                ->assertSee('invoices');
    }

     /**
     * A basic test example.
     *
     * @return void
     */
    public function test_show_parent_invoices() {
        dump('test_show_parent_invoices');
        $user = \App\Models\User::find(3);
        $child = \App\Models\ChildParent::where('parent_id',$user->profileable_id)->first();
        $record = \App\Models\Money\Invoice::create(factory(\App\Models\Money\Invoice::class)->make(['is_notify'=>1,'customer_id'=>$child->child_id,'bill_to'=>$user->profileable_id])->toArray());
        $this->actingAs($user)
            ->get('api/parents/invoices/'. $record->id)
            ->assertStatus(200)
            ->assertSee('invoices');
        $row = \App\Models\Money\Invoice::find($record->id);
        $this->assertEquals($record->customer_id, $row->customer_id);
        $record->forceDelete();
    }

}
