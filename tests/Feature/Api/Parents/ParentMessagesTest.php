<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParentMessagesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_parent_messages() {
        dump('test_list_parent_messages');

        $user = \App\Models\User::find(3);
        $this->actingAs($user)
                ->get('api/parents/messages')
                ->assertStatus(200)
                ->assertSee('messages');
    }

    public function test_create_parent_messages() {
        dump('test_create_parent_messages');

        $user = \App\Models\User::find(3);
        $row=  factory(\App\Models\Message::class)->make();
        $response=$this->actingAs($user)
                ->post('api/parents/messages', $row->toArray())
                ->assertStatus(201)
                ->assertSee('messages')
                ->assertSeeText($row->message);
        \App\Models\Message::find($response->baseResponse->original->id)->forceDelete();
    }

    // public function test_delete_parent_messages() {
    //     $user = \App\Models\User::find(4);
    //     $record=  \App\Models\Message::create(factory(\App\Models\Message::class)->make()->toArray());
    //     $row=  factory(\App\Models\Message::class)->make();
    //     $response= $this->actingAs($user)->delete('api/parents/messages/'.$record->id, $row->toArray())
    //             ->assertStatus(204);
    //     $record->forceDelete();
    // }

}
