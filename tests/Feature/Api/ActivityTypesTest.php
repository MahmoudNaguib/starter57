<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActivityTypesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_activity_types() {
        dump('test_list_activity_types');
        $user = \App\Models\User::find(2);
        $this->actingAs($user)
                ->get('api/activity_types')
                ->assertStatus(200)
                ->assertSee('activity_types');
    }


    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_show_activity_types() {
        dump('test_show_activity_types');
        $user = \App\Models\User::find(2);
        $record = \App\Models\ActivityType::create(factory(\App\Models\ActivityType::class)->make()->toArray());
        $this->actingAs($user)
                ->get('api/activity_types/'. $record->id)
                ->assertStatus(200)
                ->assertSee('activity_types');
        $row = \App\Models\ActivityType::find($record->id);
        $this->assertEquals($record->slug, $row->slug);
        $record->forceDelete();
    }
}
