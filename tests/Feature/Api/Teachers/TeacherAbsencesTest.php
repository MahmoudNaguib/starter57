<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeacherAbsencesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_teachers_absences() {
        dump('test_list_teachers_absences');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $this->actingAs($user)
                ->get('api/teachers/absences')
                ->assertStatus(200)
                ->assertSee('absences');
    }

    public function test_create_teachers_absences() {
        dump('test_create_teachers_absences');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $row = factory(\App\Models\Absence::class)->make();
        $response = $this->actingAs($user)->post('api/teachers/absences', $row->toArray())
                ->assertStatus(201)
                ->assertSee('absences')
                ->assertSeeText($row->title);
        \App\Models\Absence::find($response->baseResponse->original->id)->forceDelete();
    }

    public function test_edit_teachers_absences() {
        dump('test_edit_teachers_absences');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $record = \App\Models\Absence::create(factory(\App\Models\Absence::class)->make()->toArray());
        $row = factory(\App\Models\Absence::class)->make(['child_id'=>$record->child_id]);
        $response = $this->actingAs($user)->put('api/teachers/absences/' . $record->id, $row->toArray())
                ->assertStatus(200)
                ->assertSee('absences');
        $record->forceDelete();
    }

    public function test_delete_teachers_absences() {
        dump('test_delete_teachers_absences');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $record = \App\Models\Absence::create(factory(\App\Models\Absence::class)->make()->toArray());
        $response = $this->actingAs($user)->delete('api/teachers/absences/' . $record->id)
                ->assertStatus(204);
        $record->forceDelete();
    }

}
