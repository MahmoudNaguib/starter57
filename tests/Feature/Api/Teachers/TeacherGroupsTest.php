<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeacherGroupsTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_teachers_groups() {
        dump('test_list_teachers_groups');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $this->actingAs($user)
                ->get('api/teachers/groups')
                ->assertStatus(200)
                ->assertSee('groups');
    }

   

}
