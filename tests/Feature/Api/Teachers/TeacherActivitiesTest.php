<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeacherActivitiesTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_teachers_activities() {
        dump('test_list_teachers_activities');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $this->actingAs($user)
                ->get('api/teachers/activities')
                ->assertStatus(200)
                ->assertSee('activities');
    }

    public function test_create_teachers_activities() {
        dump('test_create_teachers_activities');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $row = factory(\App\Models\Activity::class)->make();
        $response = $this->actingAs($user)->post('api/teachers/activities', $row->toArray())
                ->assertStatus(201)
                ->assertSee('activities')
                ->assertSeeText($row->title);
        \App\Models\Activity::find($response->baseResponse->original->id)->forceDelete();
    }

    public function test_edit_teachers_activities() {
        dump('test_edit_teachers_activities');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $record = \App\Models\Activity::create(factory(\App\Models\Activity::class)->make()->toArray());
        $row = factory(\App\Models\Activity::class)->make(['child_id'=>$record->child_id]);
   
        $response = $this->actingAs($user)->put('api/teachers/activities/' . $record->id, $row->toArray())
                ->assertStatus(200)
                ->assertSee('activities');
        $record->forceDelete();
    }

    public function test_delete_teachers_activities() {
        dump('test_delete_teachers_activities');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $record = \App\Models\Activity::create(factory(\App\Models\Activity::class)->make()->toArray());
        $response = $this->actingAs($user)->delete('api/teachers/activities/' . $record->id)
                ->assertStatus(204);
        $record->forceDelete();
    }

}
