<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TeacherChildrenTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_teachers_children() {
        dump('test_list_teachers_children');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $this->actingAs($user)
                ->get('api/teachers/children')
                ->assertStatus(200)
                ->assertSee('children');
    }

     /**
     * A basic test example.
     *
     * @return void
     */
    public function test_show_teachers_children() {
        dump('test_show_teachers_children');
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
        $record = \App\Models\Child::create(factory(\App\Models\Child::class)->make()->toArray());
        $this->actingAs($user)
            ->get('api/teachers/children/'. $record->id)
            ->assertStatus(200)
            ->assertSee('children');
        $row = \App\Models\Child::find($record->id);
        $this->assertEquals($record->id, $row->id);
        $record->forceDelete();
    }
}
