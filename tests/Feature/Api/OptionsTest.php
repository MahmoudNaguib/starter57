<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OptionsTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_options() {
        $user = \App\Models\User::where(['type'=>'staff','role_id'=>2])->first();
                $this->actingAs($user)
                ->get('api/options')
                ->assertStatus(200)
                ->assertSee('options');
    }

  

}
