<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CurrenciesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_currencies() {
        dump('test_list_currencies');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Currency::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/currencies')
                ->assertStatus(200)
                ->assertSee('currencies');
    }

    public function test_create_currencies() {
        dump('test_create_currencies');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Currency::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/currencies/create', $row->toArray());
        $latest = \App\Models\Currency::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_currencies() {
        dump('test_edit_currencies');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Currency::create(factory(\App\Models\Currency::class)->make()->toArray());
        $row = factory(\App\Models\Currency::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/currencies/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Currency::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_currencies() {
        dump('test_delete_currencies');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Currency::create(factory(\App\Models\Currency::class)->make()->toArray());
        $row = factory(\App\Models\Currency::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/currencies/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
