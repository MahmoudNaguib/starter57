<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountriesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_countries() {
        dump('test_list_countries');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Country::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/countries')
                ->assertStatus(200)
                ->assertSee('countries');
    }

    public function test_create_countries() {
        dump('test_create_countries');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Country::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/countries/create', $row->toArray());
        $latest = \App\Models\Country::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_countries() {
        dump('test_edit_countries');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Country::create(factory(\App\Models\Country::class)->make()->toArray());
        $row = factory(\App\Models\Country::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/countries/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Country::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_countries() {
        dump('test_delete_countries');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Country::create(factory(\App\Models\Country::class)->make()->toArray());
        $row = factory(\App\Models\Country::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/countries/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
