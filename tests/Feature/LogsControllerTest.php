<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_logs() {
        dump('test_list_logs');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Log::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/logs')
                ->assertStatus(200)
                ->assertSee('logs');
    }

    public function test_view_logs() {
        dump('test_view_logs');
        $user = \App\Models\User::find(2);
        $data =factory(\App\Models\Log::class)->make()->toArray();
        $record = \App\Models\Log::create($data);

        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/logs/view/' . $record->id)
                ->assertStatus(200)
                ->assertSee('logs');
      $latest = \App\Models\Log::orderBy('id', 'desc')->first();
       $latest->forceDelete();
    }

    public function test_delete_logs() {
        dump('test_delete_logs');
        $user = \App\Models\User::find(2);
        $row = \App\Models\Log::create(factory(\App\Models\Log::class)->make()->toArray());
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/logs/delete/' . $row->id);
        $this->assertEquals('a', 'a');
        $row->forceDelete();
    }

}
